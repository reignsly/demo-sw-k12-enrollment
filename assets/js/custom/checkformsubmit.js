$(function(){
	var currentForm;
	
	
	$('input[type=submit]').click(function()
	{
		$(this).siblings('.current-click').removeClass('current-click');
		$(this).addClass('current-click');
	});

	
	$( '#msgDialog' ).dialog({
		draggable:false,
		autoOpen: false,
		resizable: false,
		maxWidth:600,
		maxHeight: 500,
		width: 400,
		height: 360,
		modal: true,
		buttons: {
			'No, I want to change something.' : function()
			{
				$(this).dialog('close');
			},
			'Yes, Everything is correct, proceed.': function() {
				$(window).unbind('beforeunload');
				currentForm.submit();
				// console.log(currentForm);
			}
		}
	});
	
	$( '#msgDialog2' ).dialog({
		draggable:false,
		autoOpen: false,
		resizable: false,
		maxWidth:600,
		maxHeight: 500,
		width: 400,
		height: 360,
		modal: true,
		buttons: {
			'Close.' : function()
			{
				$(this).dialog('close');
			}
		}
	});
	
	// check grade level form
	$('#check-level-form').submit(function (event) {	
		event.preventDefault();
		currentForm = this;
			
	});
	
	$('#check-level-form').on('valid', function (event) {
		var grade = $('#level-section option:selected').text();
		var block = $('input[name=section]:checked', '#check-level-form').attr('title');
		var thehtml ='';
		var block_f;
		
		if(typeof block == 'undefined')
		{
			block_f = 'No Block Set for '+ grade +'. Please Proceed.';
		}else{
			block_f = block;
		}
		thehtml += '<p>Selected grade level is: <strong>'+ grade +'</strong></p>';
		thehtml += '<p>Selected Block is: <strong>'+ block_f +'</strong></p>';
		$( '.ui-dialog-title' ).html('Grade Level and Block.');
		$( '#msgDialog > div.content' ).html(thehtml);
		$( '#msgDialog' ).dialog('open');
	});
	
	
	
	//for the main enrollment
	$('#check-form-submit').submit(function (event) {
		event.preventDefault();
		currentForm = this;
		
		$("input[type=submit].current-click", currentForm).each(function(){
			// clone the important parts of the button used to submit the form.
			if(typeof tempElement !== 'undefined')
			{
				tempElement.remove();
			}
			tempElement = $("<input type='hidden'/>");
			tempElement
				.attr("name", this.name)
				.val(true)
				.prependTo(currentForm);
		});
	});
	
	$('#check-form-submit').on('invalid', function (event){
		var message = '<p>Some Field contains invalid data.</p>';
		message += '<p><strong>Please fix the fields marked with red.</strong></p>';
		$( '.ui-dialog-title' ).html('Error.');
		$( '#msgDialog2 > div.content' ).html(message);
		$( '#msgDialog2' ).dialog('open');
	});

	$('#check-form-submit').on('valid', function (event) {
		$( '.ui-dialog-title' ).html('Verify Registration Form.');
		$( '#msgDialog > div.content' ).html('<p><strong>Please confirm that all data is true and correct.</strong></p>');
		$( '#msgDialog' ).dialog('open');
	});
	
	/*  for the resume enrollment
	--------------------------------------------------------------------*/

	$('#check-form-submit-resume').submit(function (event) {
		event.preventDefault();
		currentForm = this;

		$("input[type=submit].current-click", currentForm).each(function(){
			// clone the important parts of the button used to submit the form.
			if(typeof tempElement !== 'undefined')
			{
				tempElement.remove();
			}
			tempElement = $("<input type='hidden'/>");
			tempElement
				.attr("name", this.name)
				.val(true)
				.prependTo(currentForm);
		});
	});
		
	$('#check-form-submit-resume').on('invalid', function (event)
	{
		var type= $('#check-form-submit-resume input[type=submit].current-click').attr('id');
		
		if(type == 'save-enrollment')
		{
			var message = '<p>Some Field contains invalid data.</p>';
			message += '<p><strong>Please fix the fields marked with red.</strong></p>';
			message += '<p><strong>We advise you to add temporary data for the fields marked with RED.</strong></p>';
			message += '<p><strong>Thank You.</strong></p>';
			$( '.ui-dialog-title' ).html('Continue Enrollment Later ?.');
		}else{
			var message = '<p>Some Field contains invalid data.</p>';
			message += '<p><strong>Please fix the fields marked with red.</strong></p>';
			$( '.ui-dialog-title' ).html('Finish Enrollment ?.');
		}
		
		$( '#msgDialog2 > div.content' ).html(message);
		$( '#msgDialog2' ).dialog('open');
	});
	
	$('#check-form-submit-resume').on('valid', function (event) 
	{
		var type= $('#check-form-submit-resume input[type=submit].current-click').attr('id');
		var message = '';
		var title = '';
		
		if(type == 'save-enrollment')
		{
			title += 'Save and resume enrollment later ';
			message += '<p>This will save the enrollment data. You can resume again with the Security Key you used before.</p>';
			message += '<p>Please confirm that data provided is True and Correct.</p>';

		}else{
			
			title += 'Finish Enrollment.';
			message += '<p>This will finish the enrollment, Note if you are planning to Save and Resume the enrollment later please select no and click the Blue button.</p>';
			message += '<p><strong>This will add The student to the enrollment batch. This cannot be undone.</strong></p>';
			
		}
		
		$( '.ui-dialog-title' ).html(title);
		$( '#msgDialog > div.content' ).html(message);
		$( '#msgDialog' ).dialog('open');
	});
	
	
});