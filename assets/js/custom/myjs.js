function load_nationality()
{
	if($("input:text#nationality").val() == undefined)
	{
		//do nothing
		
	}else{
	var natVal = $("input:text#nationality").val().toLowerCase();
	var keywords = ['filipino','filipina','pilipino','pilipina','philippine','fil','pinoy','pinay','philippines','phil','fhil','pnoy'];
	
	
	// console.log(natVal == false);
	if(natVal == false)
	{
		
	}else{
		if($.inArray(natVal,keywords) == -1)
		{
			$('fieldset#for-foreign-students').show();
		}else{
			$('fieldset#for-foreign-students').hide();
		}
	}
	}
}

$(document).ready(function(){

	load_nationality();
	
	 $( ".birthdate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange:"1990:2005"
    });
	
	
	$("input:text#nationality").on('keyup change hover load blur',function(){
		var keywords = ['filipino','filipina','pilipino','pilipina','philippine','fil','pinoy','pinay','philippines','phil','fhil','pnoy'];
		var input = $(this).val().toLowerCase();
		
		if($.inArray(input,keywords) == -1)
		{
			$('div#for-foreign-students').removeClass('hidden');
			$('div#for-foreign-students input:text.nationality_must').prop('required',true);
		}else{
			$('div#for-foreign-students').addClass('hidden');
			$('div#for-foreign-students input:text.nationality_must').removeAttr('required');
			// console.log('detecting nationality is not fil');
		}
	});
	
		//siblings
		
	$( "button.add" ).button().click(function( event ) {
		event.preventDefault();
		var numItems = $('#dynapage .input-fields-clone').length;
		$('.input-fields').clone(true)
						  .attr("class", "input-fields-clone")
						  .attr("id", "input-fields-clone_"+numItems)
						  .appendTo('#dynapage')
						  .find('input[type=text]')
						  .attr("id", "input-fields-text-clone_"+numItems);
	  });
		$( "button.del" ).button().click(function( event ) {
		event.preventDefault();
		$('#dynapage .input-fields-clone:last-child').remove();
	  });
	  
	 $( "button.clrs" ).button().click(function( event ) {
		event.preventDefault();
		$('#dynapage .input-fields-clone').remove();
	  });
});// --- end document ready ---


