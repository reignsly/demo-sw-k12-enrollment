<?php

class Confirm Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout_view = 'application_no_side_bar';
		$this->load->library(array('token','create_captcha','authlink'));
		$this->footer = FALSE;
	}

	public function validate($id_raw = FALSE,$code = FALSE,$auth = FALSE)
	{	
		if($code == FALSE AND $auth == FALSE AND $id_raw == FALSE)
		{
			show_404();
		}else{
			$id = (int)substr($id_raw,0,strpos($id_raw,'_'));
			$auth_raw = $id.$id_raw.$code;
			
			if(is_numeric($id) AND $id !== 0 AND $this->authlink->check_hash_make($auth_raw,$auth))
			{
				$this->load->model('m_check_verification');
				
				$result = $this->m_check_verification->check($id,$code);
				
				$auth = $this->authlink->generate_authlink();
				
				$this->session->set_flashdata('vars',$result);
				

				if($result->stat == 'verified')
				{
					$this->_send_student_confirmation_message($result->fullname,$result->email);
					$this->session->set_userdata('enrollment_finished',FALSE);
					sleep(1);
					$this->_msg('s',' ','status/verified/'.$auth);
				}elseif($result->stat == 'expired')
				{
					$this->session->set_userdata('enrollment_finished',FALSE);
					$this->_msg('e','Verification Link Expired.','status/expired/'.$auth);
				}elseif($result->stat == 'error')
				{
					$this->session->set_userdata('enrollment_finished',FALSE);
					$this->_msg('e','error','status/error/'.$auth);
				}elseif($result->stat == 'alreadyverified')
				{
					$this->session->set_userdata('enrollment_finished',FALSE);
					show_404();
				}
			}else{
				show_404();
			}
		}
	}
	
	public function resend()
	{
		$this->view_data['token'] = $this->token->get_token();
		$this->view_data['system_message'] = $this->_msg();
		
		if($this->input->post('resend_confirmation_code'))
		{
			$this->form_validation->set_rules('email','Email Address','required|trim|htmlspecialchars|valid_email');
			$this->form_validation->set_rules('captcha_answer','captcha_answer','required|trim|htmlspecialchars');
			
			if($this->token->validate_token($this->input->post('token')) == TRUE )
			{
				if($this->create_captcha->validate($this->input->post('captcha_answer')))
				{
					if($this->form_validation->run() == TRUE)
					{
						$this->load->model('M_check_verification','mcv');
						$result = $this->mcv->resend_confirmation_code($this->input->post('email'));
						
						if($result->stat == TRUE)
						{
							$id = $result->id.'_'.rand(1000,9999);
							$c_auth = $this->authlink->hash_make($result->id.$id.$result->etc);
							$link = site_url('confirm/validate/'.$id.'/'.$result->etc.'/'.$c_auth);
						
							$this->_email_student_confirmation_link($result->fullname,$link,$result->email);
							$this->session->set_userdata('enrollment_finished',FALSE);
							sleep(1);
							$this->token->reset();
							// $this->session->set_userdata('word',$captcha->word);
							$this->_msg('s',$result->log,'confirm/resend');
						}else{
							$message = validation_errors() == '' ? NULL : validation_errors();
							$this->token->reset();
							// $this->session->set_userdata('word',$captcha->word);
							$this->_msg('e',$result->log,'confirm/resend');	
						}
					}else
					{
						$message = validation_errors() == '' ? NULL : validation_errors();
						$this->token->reset();
						$this->create_captcha->reset();
						$this->_msg('e',$message,'confirm/resend');					
					}
				}else
				{
					$this->token->reset();
					$this->create_captcha->reset();
					$this->_msg('e',' Wrong Captcha Answer','confirm/resend');
				}
			}else
			{
				$this->token->reset();
				$this->create_captcha->reset();
				$this->_msg('e','Invalid Form Submission Detected','confirm/resend');
			}
		}else{
			$this->create_captcha->_captcha();
			$captcha = $this->create_captcha->data();
			$this->view_data['question'] = $captcha->image;
		}
	}
	
	private function _send_student_confirmation_message($fullname,$email)
	{
		$school_name = ucwords($this->school_name);
		$school_name_title = $this->_accronymizer($this->school_name).'@DONOTREPLY.com';
		$student_page_link = student_portal_link();
		
		
		$data['school_name'] = $school_name;
		// $data['link'] = $link;
		$data['name'] = $fullname;
		$data['acc'] = $this->_accronymizer($this->school_name);
		
		$message = $this->load->view('email_layouts/verified_link',$data,TRUE);
		
		$this->load->library('email');
		$this->email->initialize(array( 
		   'mailtype' => 'html',
		)); 
		
		$this->email->from($school_name_title,$school_name);
		$this->email->to($email);
		$this->email->subject('Verification Successfull, Further Instructions');
		$this->email->message($message);
		$this->email->send();
	}
	
	private function _email_student_confirmation_link($fullname,$link,$email_add)
	{
		$school_name = ucwords($this->school_name);
		$school_name_title = $this->_accronymizer($this->school_name).'@DONOTREPLY.com';
		
		$data['school_name'] = $school_name;
		$data['link'] = $link;
		$data['name'] = $fullname;
		$data['acc'] = $this->_accronymizer($this->school_name);
		
		$message = $this->load->view('email_layouts/resend_confirmation_link',$data,TRUE);
		
		$this->load->library('email');
		
		$this->email->initialize(array( 
		   'mailtype' => 'html', 
		)); 
		
		$this->email->from($school_name_title,$school_name);
		$this->email->to($email_add);
		$this->email->subject('Online Enrollment: Resend Verification Link');
		$this->email->message($message);
		$this->email->send();
	}
	
	/*
		input University of the great and noble
		ouput UOTGAN
	*/
	private function _accronymizer($string = FALSE)
	{
		if($string !== FALSE)
		{
			$clean_string = htmlspecialchars(strip_tags($string));
			$number_of_words = str_word_count($clean_string,1);
			$accronym = array_map(function($x){
				if(strlen($x) > 1)
				{
					return $x[0];
				}
			},
			$number_of_words);
			return strtoupper(implode('',$accronym));
		}else{
			return NULL;
		}
	}
}