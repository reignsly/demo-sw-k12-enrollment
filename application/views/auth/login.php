<div class="ui-corner-all captionHeader" style="background-color: #ffff5f;">
<strong>Student Login</strong>
</div>

<div class="ui-corner-all contentContainer" style="background-color: #fff;">
	<div style="margin: 0px auto; width: 250px;">
	<?=form_open()?>

	<?php if($this->session->flashdata('system_message')) echo '<div class="error">'.$this->session->flashdata('system_message').'</div>';?>
	<?php if(validation_errors() !== '') echo '<div class="error">'.validation_errors().'</div>';?>

	<div class="contentInput">
		Username: <?php $opt = 'class="ui-corner-all" style="border:#ccc 2px solid;" maxlength="255"'; echo form_input('username', set_value('username'), $opt); ?>
	</div>
	<div class="contentInput">
		<span style="padding-right: 2px;">Password:</span> <?php echo form_password('password','',$opt); ?>
	</div>
	<div style="text-align: center;">

	<?php 
	$opt4 = 'class="ui-corner-all submit"';
	echo form_submit('submit', 'Login', $opt4);
	echo form_close(); ?>
	</div>

	</div>
</div>