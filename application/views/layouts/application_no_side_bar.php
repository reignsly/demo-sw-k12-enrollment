<!doctype html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
    <title>school system</title>
	<link rel="stylesheet" href="<?=style_url('normalize');?>">
	<link rel="stylesheet" href="<?=style_url('foundation.min');?>">
	<link rel="stylesheet" href="<?=style_url('bootstrap.min');?>">
	<link rel="stylesheet" href="<?=style_url('style');?>">
	<link rel="stylesheet" href="<?=style_url('jquery.smoothness.ui');?>">
	
    <script src="<?php echo script_url('custom/jquery1.9.1'); ?>">
	<script>window.jQuery || document.write('<script src="<?php echo script_url('vendor/jquery'); ?>"><\/script>')</script>
    <script type="text/javascript" src="<?php echo script_url('vendor/custom.modernizr');?>"></script>

	</head>
	
    <body>
        <!-- message dialog box -->
		<!-- start contents -->
		<div class="fixed">
		<nav class="top-bar">
		  <ul class="title-area">
			<li class="name">
				<h1><a href="#"><?php echo $this->school_name !== '' ?$this->school_name: '';?></a></h1>
			</li>
		  </ul>
			<section class="top-bar-section">
				<ul>
					<li><a href="#">Enrollment For School year <?=$this->schoolyear_date;?></a></li>
				</ul>
			</section>
		</nav>
		</div>
		
		<div class="row">
			<noscript>
				<div class="error">Javascript has been disabled on your browser.some Functions may not work</div>
			</noscript>

			<?if(isset($logo_message)):?>
				<div class="alert-box" style="margin:25px 0;">
				<?=$logo_message?>
				<?if(isset($demo_account) && $demo_account === TRUE):?>
					<a href="<?=site_url('system_page_auth/logout')?>" class="btn btn-mini btn-danger confirm" title="Are you sure you want to exit?"><i class="fa fa-power-off"></i>&nbsp; Logout</a>
				<?endif;?>
				</div>
			<?endif;?>

			<?if(isset($browser_name)):?>
				<?if(@$browser_name == 'Internet Explorer'):?>
					<div class="alert-box" style="margin:50px 0;">
						<h4>Notice.</h4>
						<p>System has detected you are using Internet Explorer. <?=@$browser_vers;?></p>
						<p>We encourage you to use <a href="https://www.google.com/intl/en/chrome/browser/" class="btn btn-success btn-mini" target="_blank">Google Chrome</a> or <a href="http://www.mozilla.org/en-US/firefox/new/" class="btn btn-success btn-mini" target="_blank">Mozilla Firefox</a> as the system is currently not suitable to use under Internet Explorer.</p>
						<p>We are sorry for the inconvinience, we are trying to fix this as soon as possible.</p>
					</div>
				<?else:?>
					<div style="margin:10px 0;">
					<?php echo $yield;?>
					</div>
				<?endif;?>
			<?else:?>
				<div style="margin:10px 0;">
					<?php echo $yield;?>
				</div>
			<?endif;?>
		</div>
		<?if(isset($footer)):?>
			<?if($footer):?>
			<footer>
				<div class="row footer-contents">
					copyright &copy; gocloudasia 2013
				</div>
			</footer>
			<?endif;?>
		<?endif;?>
		<!-- End contents -->
		<script type="text/javascript" src="<?php echo script_url('custom/checkformsubmit');?>"></script>
		<script>
		  document.write('<script src=' +
		  ('__proto__' in {} ? '<?=script_url("vendor/zepto");?>' : '<?=script_url("vendor/jquery");?>') +
		  '><\/script>')
		</script>
		<script type="text/javascript" src="<?php echo script_url('custom/jquery-ui-1.10.2');?>"></script>
		<script>window.jQuery.ui || document.write('<script src="<?php echo script_url('vendor/jquery.ui'); ?>"><\/script>')</script>
		<script type="text/javascript" src="<?php echo script_url('foundation.min');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/myjs'); ?>"></script>
		<script src="<?=script_url('foundation/foundation');?>"></script>
		<script src="<?=script_url('foundation/foundation.alerts');?>"></script>
		<script src="<?=script_url('foundation/foundation.dropdown');?>"></script>
		<script src="<?=script_url('foundation/foundation.forms');?>"></script>
		<script src="<?=script_url('foundation/foundation.tooltips');?>"></script>
		<script src="<?=script_url('foundation/foundation.topbar');?>"></script>
		<script src="<?=script_url('foundation/foundation.abide');?>"></script>
		
		<script>
		  $(document).foundation();
		</script>
		
		<div id="msgDialog" style="font-size:12px;">
		<p class="title" style="font-weight:bold;"></p>
		<div class="content"></div>
		</div>
		<div id="msgDialog2" style="font-size:12px;">
		<p class="title" style="font-weight:bold;"></p>
		<div class="content"></div>
		</div>
	</body>
</html>