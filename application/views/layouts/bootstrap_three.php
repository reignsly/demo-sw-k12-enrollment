<!doctype html>
<html>
    <head>
	<link rel="icon" type="image/ico" href="<?=base_url();?>/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
    <title><?=isset($title)? $title: 'School System';?></title>
	<link rel="stylesheet" href="<?=style_url('bootstrap.minv3.2.0');?>"> 
	<link rel="stylesheet" href="<?=style_url('bootstrap-theme.v3.2.0.min');?>">
	<link rel="stylesheet" href="<?=style_url('style');?>">
	<link rel="stylesheet" href="<?=style_url('jquery.smoothness.ui');?>"> <!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"> -->
    <script type="text/javascript" src="<?php echo script_url('vendor/custom.modernizr');?>"></script>
	<script src="<?php echo script_url('custom/jquery1.9.1'); ?>">
	<script>window.jQuery || document.write('<script src="<?php echo script_url('vendor/jquery'); ?>"><\/script>')</script>
    </head>
	
    <body>
    
    <div class="content">
		
			<div class="row">
				<noscript>
					<div class="error">Javascript has been disabled on your browser.some Functions may not work</div>
				</noscript>
				<?php echo $yield;?>
			</div>

		</div> 

		<footer class="footer hide-for-small">
			<div class="footer-contents center">
				<span class="btn btn-mini"> copyright &copy; <span class="complogo_name1">GoCloud</span><span class="complogo_name2">Asia</span> - 2013</span> 
				<span class="btn btn-mini benchmark">Time Exec: <?=isset($elapsed_time) ? $elapsed_time: '0.0';?></span> 
				<span class="btn btn-mini benchmark">Memory: <?=isset($memory_usage) ? $memory_usage : '0.0';?></span>
			</div>
		</footer>

		<!-- End contents -->
		<script type="text/javascript" src="<?php echo script_url('custom/bootstrapv3.2.0'); ?>"></script>
		<script>window.jQuery || document.write('<script src="<?php echo script_url('vendor/jquery'); ?>"><\/script>')</script>
		<script>
		  document.write('<script src=' +
		  ('__proto__' in {} ? '<?=script_url("vendor/zepto");?>' : '<?=script_url("vendor/jquery");?>') +
		  '><\/script>')
		</script>
		<script type="text/javascript" src="<?php echo script_url('custom/jquery-ui-1.10.2');?>">
		<script type="text/javascript" src="<?php echo script_url('foundation.min');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/myjs'); ?>"></script>
		<script src="<?=script_url('foundation/foundation');?>"></script>
		<script src="<?=script_url('foundation/foundation.forms');?>"></script>
		<script src="<?=script_url('foundation/foundation.reveal');?>"></script>
		<script src="<?=script_url('foundation/foundation.section');?>"></script>
		<script src="<?=script_url('foundation/foundation.dropdown');?>"></script>
		<script src="<?=script_url('foundation/foundation.tooltips');?>"></script>
		<script src="<?=script_url('foundation/foundation.topbar');?>"></script>
		<script>
		  $(document).foundation();
		</script>
		<div id="msgDialog"><p></p></div>
	</body>
</html>