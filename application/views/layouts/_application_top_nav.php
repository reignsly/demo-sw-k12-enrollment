<?php

$fname = $this->session->userdata('e_f_name');
$user_id = $this->session->userdata('userid');
$lname = $this->session->userdata('e_l_name');
$cp_authlink = $this->authlink->generate_authlink();
$mname = $this->session->userdata('e_m_name');
$usertype = $this->session->userdata('userType');
?>
<nav class="top-bar">
  <section class="top-bar-section">
    <!-- Left Nav Section -->
	<ul class="title-area">
					<!-- Title Area -->
					<li class="name">
					  <h1>
					  <a href="#"><?php echo $this->school_name !== '' ?$this->school_name: '';?></a>
					  </h1>
					</li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				  </ul>

				  <section class="top-bar-section">
					 <ul class="left">
						<li class="divider"></li>
						<li class="name">
							<h1><a href="#">(<?=ucwords($usertype);?> Account)</a></h1>
						</li>
					</ul>
				  </section>

    <!-- Right Nav Section -->
    <ul class="right">
      <li class="divider hide-for-small"></li>
      <li class="has-dropdown"><a href="#"><a href="#"><i class="icon-user icon-white"></i> <?=ucwords($fname.' '.$mname[0].'. '.$lname);?></a>

        <ul class="dropdown">
		  <li class="left"><a href="<?php echo site_url();?>"><i class='icon-white icon-home'></i> Home</a></li>
          <li class="left"><a href="<?php echo site_url('change/password/'.$user_id.'/'.$cp_authlink);?>"><i class='icon-white icon-edit'></i> Change Password</a></li>
          <li class="divider"></li>
          <li class="left"><a href="<?php echo site_url('auth/logout');?>"><i class='icon-white icon-off'></i> Logout </a></li>
        </ul>
      </li>
  </section>
</nav>