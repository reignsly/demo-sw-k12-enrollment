<?
echo doctype();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
    <title>school system</title>
	<link rel="stylesheet" href="<?=style_url('normalize');?>">
	<link rel="stylesheet" href="<?=style_url('foundation.min');?>">
	<link rel="stylesheet" href="<?=style_url('bootstrap.min');?>">
	<link rel="stylesheet" href="<?=style_url('style');?>">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css">
	
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?php echo script_url('vendor/custom.modernizr');?>"></script>
    </head>
	

    <body>
        <!-- message dialog box -->
		<!-- start contents -->
		<nav class="top-bar">
		  <ul class="title-area">
			<li class="name">
			<h1><a href="#"><?php echo $this->school_name !== '' ?$this->school_name: '';?></a></h1>
			</li>
		  </ul>
		</nav>
		
		<div class="row content-views shadow">
			<noscript>
				<div class="error">Javascript has been disabled on your browser.some Functions may not work</div>
			</noscript>
				<?php echo $yield;?>
		</div>
		<footer>
			<div class="row footer-contents">
				copyright &copy; gocloudasia 2013
			</div>
		</footer>
		<!-- End contents -->
		<script>
		  document.write('<script src=' +
		  ('__proto__' in {} ? '<?=script_url("vendor/zepto");?>' : '<?=script_url("vendor/jquery");?>') +
		  '><\/script>')
		</script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo script_url('foundation.min');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('vendor/timpicker.addon');?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/myjs'); ?>"></script>
		<script type="text/javascript" src="<?php echo script_url('custom/calendarscript'); ?>"></script>	
		<script type="text/javascript" src="<?php echo script_url('custom/cash'); ?>"></script>
		<script src="<?=script_url('foundation/foundation');?>"></script>
		<script src="<?=script_url('foundation/foundation.alerts');?>"></script>
		<script src="<?=script_url('foundation/foundation.clearing');?>"></script>
		<script src="<?=script_url('foundation/foundation.cookie');?>"></script>
		<script src="<?=script_url('foundation/foundation.dropdown');?>"></script>
		<script src="<?=script_url('foundation/foundation.forms');?>"></script>
		<script src="<?=script_url('foundation/foundation.joyride');?>"></script>
		<script src="<?=script_url('foundation/foundation.magellan');?>"></script>
		<script src="<?=script_url('foundation/foundation.orbit');?>"></script>
		<script src="<?=script_url('foundation/foundation.placeholder');?>"></script>
		<script src="<?=script_url('foundation/foundation.reveal');?>"></script>
		<script src="<?=script_url('foundation/foundation.section');?>"></script>
		<script src="<?=script_url('foundation/foundation.tooltips');?>"></script>
		<script src="<?=script_url('foundation/foundation.topbar');?>"></script>
		<script>
		  $(document).foundation();
		</script>
		<div id="msgDialog"><p></p></div>
	</body>
</html>