<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoPanel - Backend</title>
		<?/*** LAYOUT REF : http://startbootstrap.com/template-overviews/sb-admin-2/ */?>
    <link rel="stylesheet" href="<?=style_url('bootstrap.minv3.2.0');?>"> 
		<link rel="stylesheet" href="<?=style_url('bootstrap-theme.v3.2.0.min');?>">
		
		<link rel="stylesheet" href="<?=style_url('jquery.smoothness.ui');?>">
    <link href="<?=site_url('assets/gopanel/css/metisMenu.min.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/gopanel/css/sb-admin-2.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/css/fontawesome.min.css')?>" rel="stylesheet">
		
		<script src="<?php echo script_url('custom/jquery1.9.1'); ?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">GoPanel - Backend</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li> -->
                        <li><a href="<?=site_url('gopanel_auth/logout')?>"><i class="fa fa-power-off"></i>&nbsp; Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        </li> -->
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Master Files<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=site_url('gopanel/system_users')?>"><i class="fa fa-users"></i>&nbsp; System Users</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
	                    <?=isset($page_title) ? $page_title : ucwords(str_replace('_', ' ', $this->router->class)) ?>
											<?if(isset($custom_title) && $custom_title):?>
												<small><?=$custom_title;?></small>
											<?endif;?>
										</h1>
                </div>
                <div class="col-lg-12">
                  <?=isset($system_message)?$system_message:"";?>
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12"><?=$yield?></div>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
		<script type="text/javascript" src="<?php echo script_url('custom/jquery-ui-1.10.2');?>"></script>
    <script type="text/javascript" src="<?php echo script_url('custom/bootstrapv3.2.0'); ?>"></script>
    <script src="<?=site_url('assets/gopanel/js/metisMenu.min.js')?>"></script>
    <script src="<?=site_url('assets/gopanel/js/sb-admin-2.js')?>"></script>
    <script type="text/javascript" src="<?php echo script_url('custom/myjs'); ?>"></script>

</body>

</html>