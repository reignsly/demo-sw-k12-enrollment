<div class="row panel" style="border:1px solid #000;">
	<div class="large-12 small-12 columns">
		<p class="alert-box success"><strong>Congratulations, Successfully verifed your confirmation link.</strong></p>
		<p>Please check your email address <strong><?=strtolower(@$email);?></strong> for further instructions.</p>
		<p>Thank You.</p>
	</div>
	<div class="large-12 small-12 columns">
		<div style="border:3px solid #c0c0c0;padding:10px;">
			<p style="font:15px bold;"><i class="icon-warning-sign"></i>&nbsp;&nbsp;NOTE</p>
			<p style="font:12px bold;">What to do if no message received?</p>
			<ul style="font:12px bold;">
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;Please Check your spam folder.</li>
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;Check for other Tabs.</li>
				<li><i class="icon-chevron-right"></i>&nbsp;&nbsp;(If email forwarding is enabled on your email) Please Check the email address it was forwarded to.</li>
			</ul>
		</div>
	</div>
</div>