<div class="row">
	<div class="large-12 small-12 columns">
		<div style="border:3px solid #c0c0c0;padding:10px;">
			<h2 class="label alert"><i class="icon-warning-sign"></i>&nbsp;&nbsp;An Error was Encountred.</h2>
			<p style="font:12px bold;">Sorry but our system has encountered an untimely error.</p>
			<ul style="font:12px bold;">
				<li>Please Try Again Later.</li>
				<li>This error has been logged to Ticket Number: <strong><?=$ticket;?></strong>, Please Contact Administrator and provide ticket number.</li>
			</ul>
		</div>
	</div>
</div>