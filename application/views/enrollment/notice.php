<div class="panel">
	<i class="icon-info-sign"></i>&nbsp;&nbsp;<strong>Things To Remember</strong>
	<ul class="notice">
		<li>&nbsp;</li>
		<li><strong>Please use a valid email Address since your account credentials will be sent there.</strong></li>
		<li><strong>New Students Will be required to visit the School for Personal Interview.</strong></li>
		<li><strong>If a question is not applicable to you just write <code>none</code></strong>.</li>
		<li><strong>For telephone or any numeric fields type (zero) <code>0</code> if none</strong>.</li>
		<li><strong>Questions with <code>&#10033;</code> are required.</strong>.</li>
	</ul>
</div>