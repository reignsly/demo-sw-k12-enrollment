<style>
	div.enrollment-menu
	{
		padding:20px;
		border:2px dashed #c0c0c0;
	}
	div.option{
		margin:20px 5px;
	}
</style>

<div class="row">
	<div class="large-3 columns">&nbsp;</div>
	
	<div class="large-6 columns enrollment-menu content-views">
					<div class="alert-box">
						To verify that you are student no <span class="bold"><?=$student_id;?></span> Please fill out the data below carefully.
					</div>
					<?if(isset($system_message))echo $system_message;?>
					<form action="<?=site_url('enrollment/verify/old/'.$student_id.'/'.$auth.'/'.$hash);?>" method="POST" autocomplete="off" >
					<fieldset>
						<legend>Enrollment Credentials</legend>
							<div class="input-block">
								<label>Firstname:</label>
								<input type="text" name="fname" value="" autocomplete="off" required>
							</div>
							<div class="input-block">
								<label>Lastname:</label>
								<input type="text" name="lname" value="" autocomplete="off" required>
							</div>
							<div class="input-block">
								<label>Student Portal Password:</label>
								<input type="password" name="password" value="" autocomplete="off" required>
							</div>
							
					</fieldset>
					<fieldset>
						<legend>Captcha</legend>
						<div class="input-block">
							<label><?=$question;?></label>
							<input type="text" name="captcha_answer" autocomplete="off" required>
						</div>
					
					</fieldset>
						<div class="input-block">
							<label>&nbsp;&nbsp;</label>
							<input type="hidden" name="form_token" value="<?=$form_token;?>">
							<input type="submit" class="btn btn-primary" name="verify_old_enrollee" value="Verify And Continue Enrollment">
						</div>
					</form>
	</div>
	
	<?echo form_close()?>
	<div class="large-3 columns">&nbsp;</div>
</div>