<?
//prep data for enroll again if enrolled again is enabled
if(isset($enrollagain))
{
	$enrollagain = (object)$enrollagain;

	$present_address = isset($enrollagain->present_address) ? $enrollagain->present_address : '';
	$contact_no = isset($enrollagain->contact_no) ? $enrollagain->contact_no : '';
	$email = isset($enrollagain->email) ? $enrollagain->email : '';
	$how_long_living_present_address = isset($enrollagain->how_long_living_present_address) ? $enrollagain->how_long_living_present_address : '';
	$last_school_name = isset($enrollagain->last_school_name) ? $enrollagain->last_school_name : '';
	$last_school_level = isset($enrollagain->last_school_level) ? $enrollagain->last_school_level : '';
	$last_school_address = isset($enrollagain->last_school_address) ? $enrollagain->last_school_address : '';
	$fathername = isset($enrollagain->fathername) ? $enrollagain->fathername : '';
	$father_age = isset($enrollagain->father_age) ? $enrollagain->father_age : '';
	$father_religion = isset($enrollagain->father_religion) ? $enrollagain->father_religion : '';
	$father_nationality = isset($enrollagain->father_nationality) ? $enrollagain->father_nationality : '';
	$father_educ_attain = isset($enrollagain->father_educ_attain) ? $enrollagain->father_educ_attain : '';
	$father_hobby_talent = isset($enrollagain->father_hobby_talent) ? $enrollagain->father_hobby_talent : '';
	$father_occupation = isset($enrollagain->father_occupation) ? $enrollagain->father_occupation : '';
	$father_office_add = isset($enrollagain->father_office_add) ? $enrollagain->father_office_add : '';
	$father_office_num = isset($enrollagain->father_office_num) ? $enrollagain->father_office_num : '';
	$mothername = isset($enrollagain->mothername) ? $enrollagain->mothername : '';
	$mother_age = isset($enrollagain->mother_age) ? $enrollagain->mother_age : '';
	$mother_religion = isset($enrollagain->mother_religion) ? $enrollagain->mother_religion : '';
	$mother_nationality = isset($enrollagain->mother_nationality) ? $enrollagain->mother_nationality : '';
	$mother_educ_attain = isset($enrollagain->mother_educ_attain) ? $enrollagain->mother_educ_attain : '';
	$mother_hobby_talent = isset($enrollagain->mother_hobby_talent) ? $enrollagain->mother_hobby_talent : '';
	$mother_occupation = isset($enrollagain->mother_occupation) ? $enrollagain->mother_occupation : '';
	$mother_office_add = isset($enrollagain->mother_office_add) ? $enrollagain->mother_office_add : '';
	$mother_office_num = isset($enrollagain->mother_office_num) ? $enrollagain->mother_office_num : '';
	$guardian_name = isset($enrollagain->guardian_name) ? $enrollagain->guardian_name : '';
	$relationship = isset($enrollagain->relationship) ? $enrollagain->relationship : '';
	$guardian_address = isset($enrollagain->guardian_address) ? $enrollagain->guardian_address : '';
	$guardian_contact_no = isset($enrollagain->guardian_contact_no) ? $enrollagain->guardian_contact_no : '';
	$guardian_reason = isset($enrollagain->guardian_reason) ? $enrollagain->guardian_reason : '';
	$parent_status = isset($enrollagain->parent_status) ? $enrollagain->parent_status : '';
	$parent_status_how_long = isset($enrollagain->parent_status_how_long) ? $enrollagain->parent_status_how_long : '';
	$parent_rights = isset($enrollagain->parent_rights) ? $enrollagain->parent_rights : '';
	$parents_rights_who = isset($enrollagain->parents_rights_who) ? $enrollagain->parents_rights_who : '';
}



$year = range(1990,date('Y')-2);
$year_d[''] = 'Year';
foreach($year as $k => $v)
{
	$year_d[$v] = $v;
}
$month = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
$count = 1;
$month_d[''] = 'Month';
foreach($month as $v)
{
	$month_d[$count++] = ucwords($v);
}
?>


<?php
$genderAttrib = array('' => 'Select Gender...', 'male' => 'Male', 'female' => 'Female' );
$child_birth_position = array(''=>'-- Select --','first'=>'First','Second'=>'Second','third'=>'Third','fourth'=>'Fourth','fifth'=>'Fifth','sixth'=>'Sixth','youngest'=>'youngest','only'=>'Only');
?>
<div class="large-12 columns enrollment-menu content-views">
<?=$system_message;?>
<div class="alert-box">REGISTRATION FORM Part 1: Student Personal Information And Family History</div>
<?echo form_open('register','id="check-form-submit" class="custom" data-abide autocomplete="off"');?>
<?$this->load->view('enrollment/notice')?>
<!-- start student personal info -->
	<legend class="label" style="margin:0 auto;">Student Personal Information</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">Valid Email Address<span>&#10033;</span></label>
			<?='<br>'.form_error('email');?>
			<input type="email" data-tooltip other="Email Address" class="has-tip tip-top" title="Please provide a valid e-mail address where Student Information, Credentials and Other News will be sent." name="email" value="<?=set_value('email',@$email);?>" required>
			<small class="error">Email is required and must be a valid email.</small>
		</div>
	</div>
	<div class="clearfix"></div>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">First Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_fname');?>
			<input type="text" name="child_fname" other="childs First name" value="<?=set_value('child_fname');?>" required>
			<small class="error">Firstname is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Middle Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_mname') == ''?'<span style="font-size:10px;font-weight:bold;">If No middle name just put "none"</span>':'<br>'.form_error('child_mname');?>
			<input type="text" name="child_mname" other="Child's Middle name" id="Middlename" value="<?=set_value('child_mname');?>" required>
			<small class="error">Middle name is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Last Name<span>&#10033;</span></label>
			<?='<br>'.form_error('child_lname');?>
			<input type="text" name="child_lname" other="Child's Last Name" value="<?=set_value('child_lname');?>" required>
			<small class="error">Middle name is required.</small>
		</div>
	</div>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">Nickname</label>
			<?='<br>'.form_error('child_nickname');?>
			<input type="text" name="child_nickname" other="child's Nickname/Preffered Name" value="<?=set_value('child_nickname');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Name Ext (e.g jr,II,III)</label>
			<?='<br>'.form_error('child_name_ext');?>
			<input type="text" name="child_name_ext" other="Name Ext" value="<?=set_value('child_name_ext');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Place of Birth</label>
			<?='<br>'.form_error('child_placeofbirth');?>
			<input type="text" name="child_placeofbirth" other="Place of Birth" value="<?=set_value('child_placeofbirth');?>">
			<small class="error">Place of Birth is required.</small>
		</div>
	</div>
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Childs Birthdate:</label>
			<!--<<?='<br>'.form_error('child_bdate');?>
			<input type="text" name="child_bdate" placeholder="MM/DD/YYYY" other="birthdate" class="birthdate" value="<?=set_value('child_bdate');?>" pattern="month_day_year" >
			<small class="error">Birthdate is required. and format must be MM/DD/YYYY</small>-->
			<?='<br>'.form_error('b_day');?>
			<div class="row">
				<div class="large-4 small-4 columns">
					<?=form_dropdown('b_month',$month_d,set_value('b_month'),'');?>
					<small class="error">Month is required</small>
				</div>
				<div class="large-4 small-4  columns">
					<input type="text" name="b_day" value="<?=set_value('b_day');?>" placeholder="Day" pattern=[0-9] >
					<small class="error">Day us required and must be numeric</small>
				</div>
				<div class="large-4  small-4  columns">
					<?=form_dropdown('b_year',$year_d,set_value('b_year'),'');?>
					<small class="error">Year is required</small>
				</div>
			</div>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Gender</label>
			<?='<br>'.form_error('gender');?>
			<?=form_dropdown('gender', $genderAttrib, set_value('gender'),'other="Gender" ');?>
			<small class="error">Gender is required.</small>
		</div>
		<div class="large-3 columns">
			<label class="radius secondary label">Religious Affiliation</label>
			<?='<br>'.form_error('child_religous');?>
			<input type="text" name="child_religous" other="Religous Affilation" value="<?=set_value('child_religous');?>">
			<small class="error">Religous Affilation is required.</small>
		</div>
	</div>
	<span class="clearfix"></span>
	<legend class="label" style="margin:15px 0;">Nationality Information</legend>
	<div>
		<div>
			<?=form_error('child_nationality');?>
		</div>
		<div class="clearfix"></div>
		<div class="large-4 columns">
			
			<label class="radius secondary label">Nationality</label>
			<input type="text" id="nationality" name="child_nationality" other="nationality"  value="<?=set_value('child_nationality','filipino');?>">
			<small class="error">Nationality is required.</small>
		</div>
		
		<div class="clearfix"></div>
		<div id="for-foreign-students" class="hidden">
			<hr class="clearfix" style="border:2px solid #2BA6CB;">
			<legend class="label" style="margin:0 auto;">For Foreign Students ONLY</legend>
			<br class="clearfix">
			<div>
				<div class="large-4 columns">
					<label class="radius secondary label">SSP Number</label>
					<?='<br>'.form_error('nationality_ssp_number');?>
					<input type="text" name="nationality_ssp_number" class="nationality_must" other="SSP Number (if foreign only)" value="<?=set_value('nationality_ssp_number');?>">
					<small class="error">SSP Number is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">VISA Status</label>
					<?='<br>'.form_error('nationality_visa_status');?>
					<input type="text" name="nationality_visa_status" class="nationality_must" other="Visa Status(if foreign only)"value="<?=set_value('nationality_visa_status');?>">
					<small class="error">VISA Status is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Authorized Stay</label>
					<?='<br>'.form_error('nationality_auth_stay');?>
					<input type="text" name="nationality_auth_stay" class="nationality_must" other="Authorized stay (if foreign only)" value="<?=set_value('nationality_auth_stay');?>">
					<small class="error">Authorized stay is required.</small>
				</div>
			</div>
			<div>
				<div class="large-4 columns">
					<label class="radius secondary label">Passport No.</label>
					<?='<br>'.form_error('nationality_passport_no');?>
					<input type="text" name="nationality_passport_no" class="nationality_must" other="Passport number(if foreign only)" value="<?=set_value('nationality_passport_no');?>">
					<small class="error">Passport No. is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">I-Card No.</label>
					<?='<br>'.form_error('nationality_icard_no');?>
					<input type="text" name="nationality_icard_no" class="nationality_must" other="I-Card (if foreign only)" value="<?=set_value('nationality_icard_no');?>">
					<small class="error">I-card is required.</small>
				</div>
				<div class="large-4 columns">
					<label class="radius secondary label">Date of Issue</label>
					<?='<br>'.form_error('nationality_date_issued');?>
					<input type="text" name="nationality_date_issued" class="nationality_must" other="Date of Issue (if foreign only)" class="datepicker" value="<?=set_value('nationality_date_issued');?>">
					<small class="error">Date of issue is required and must be a valid date MM/DD/YYYY</small>
				</div>
			</div>
			<hr class="clearfix" style="border:2px solid #2BA6CB;">
		</div>
	</div>
	<legend class="label">Current Address Information</legend>
	<div style="margin:10px 0;">
		<div class = "large-2 columns">
			<label class="radius secondary label">City Address</label>
			<?='<br>'.form_error('h_no');?>
		      <input type = "text" class = "form-control" name = "h_no" placeholder = "House #" value = "<?=set_value('h_no')?>">
		      <small class="error">City Address is required.</small>
		  </div>
		  <div class = "large-2 columns text-right">
		  	<label class="radius secondary label">City Address</label>
			<?='<br>'.form_error('st_no');?>
		      <input type = "text" class = "form-control" name = "st_no" placeholder = "Street #"  value = "<?=set_value('st_no')?>">	       
		      <small class="error">City Address is required.</small>
		  </div>
		  <div class = "large-2 columns">
		  	<label class="radius secondary label">City Address</label>
			<?='<br>'.form_error('brgy');?>
		      <input type = "text" class = "form-control" name = "brgy" placeholder = "Barangay" value = "<?=set_value('brgy')?>">
		      <small class="error">City Address is required.</small>
		  </div>
		  <div class = "large-3 columns">
		  	<label class="radius secondary label">City Address</label>
			<?='<br>'.form_error('brgy');?>
		      <input type = "text" class = "form-control" name = "munc" placeholder = "Municipality" value = "<?=set_value('munc')?>">
		      <small class="error">City Address is required.</small>
		  </div>
		  <div class = "large-3 columns">
		  	<label class="radius secondary label">City Address</label>
			<?='<br>'.form_error('city');?>
		      <input type = "text" class = "form-control" name = "city" placeholder = "City" value = "<?=set_value('city')?>">
		      <small class="error">City Address is required.</small>
		  </div>

		<div class="large-4 columns">
			<label class="radius secondary label">Telephone Number</label>
			<?='<br>'.form_error('child_telno');?>
			<input type="text" name="child_telno" other="Contact Number" value="<?=set_value('child_telno',@$contact_no);?>" >
			<small class="error">Contact Number is required.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label" style = "font-size:12px;">How Long has Child Lived in This Address?</label>
			<?='<br>'.form_error('child_adresshowlong');?>
			<input type="text" name="child_adresshowlong" other="How long has child been living in this address" value="<?=set_value('child_adresshowlong',@$how_long_living_present_address);?>">
			<small class="error">How long child has been living in this address is required.</small>
		</div>
	</div>
	<hr>
	<legend class="label" style="margin:10px 0;">Recent School Information</legend>
	<div>
		<div class="large-4 columns">
			<label class="radius secondary label">School Last Attended></label>
			<?='<br>'.form_error('school_last_attended');?>
			<input type="text" name="school_last_attended" other="School last attended" value="<?=set_value('school_last_attended',@$last_school_name);?>" >
			<small class="error">This field is xequired.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">School Level Completed</label>
			<?='<br>'.form_error('school_level_completed');?>
			<input type="text" name="school_level_completed" other="School level completed" value="<?=set_value('school_level_completed',@$last_school_level);?>">
			<small class="error">This field is xequired.</small>
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Address Of School</label>
			<?='<br>'.form_error('school_address');?>
			<input type="text" name="school_address" other="School Address" value="<?=set_value('school_address',@$last_school_address);?>" >
			<small class="error">This field is xequired.</small>
		</div>
	</div>
	<div>
		<legend class="label" style="margin:10px 0;">Information About the Father</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Full Name</label>
				<?='<br>'.form_error('father_name');?>
				<input type="text" name="father_name" other="Father's Name" value="<?=set_value('father_name',@$fathername);?>">
				<small class="error">This field is xequired.</small>
			</div class="large-4 columns">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Age </label>
				<?='<br>'.form_error('father_age');?>
				<input type="text" name="father_age"other="Father's Age" maxlength="3" value="<?=set_value('father_age',@$father_age);?>" >
				<small class="error">This field is required and must be numeric.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Religous Affilation:</label>
				<?=form_error('father_relaffil');?>
				<input type="text" name="father_relaffil" other="Father's Religious Affilation" value="<?=set_value('father_relaffil',@$father_religion);?>">
				<small class="error">This field is xequired.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Citizenship:</label>
				<?='<br>'.form_error('father_citizenship');?>
				<input type="text" name="father_citizenship" other="Fathers Citizenship" value="<?=set_value('father_citizenship',@$father_nationality);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Fathers's Educational Attainment</label>
				<?='<br>'.form_error('father_educ');?>
				<input type="text" name="father_educ" other="Father's Educational Attainment" value="<?=set_value('father_educ',@$father_educ_attain);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Hobbies/Talent</label>
				<?='<br>'.form_error('father_talent');?>
				<input type="text" name="father_talent" other="Father's Hobbies/Talent" value="<?=set_value('father_talent',@$father_hobby_talent);?>">
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Occupation/Position</label>
				<?='<br>'.form_error('father_occup');?>
				<input type="text" name="father_occup" other="Father's Occupation/Position" value="<?=set_value('father_occup',@$father_occupation);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Office Address</label>
				<?='<br>'.form_error('father_office_address');?>
				<input type="text" name="father_office_address" other="Father's Office Address" value="<?=set_value('father_office_address',@$father_office_add);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Father's Office Telephone Number</label>
				<?='<br>'.form_error('father_office_tel');?>
				<input type="text" name="father_office_tel" other="Father's Office Contact Number" value="<?=set_value('father_office_tel',@$father_office_num);?>">
				<small class = "error">Should be numeric. If none input '0'</small>
			</div>
		</div>
	</div>
	
	<div>
		<legend class="label" style="margin:10px 0;">Information about the Mother</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Full Name</label>
				<?='<br>'.form_error('mother_name');?>
				<input type="text" name="mother_name" other="Mother's Name" value="<?=set_value('mother_name',@$mothername);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Age</label>
				<?='<br>'.form_error('mother_age');?>
				<input type="text" name="mother_age" other="Mother Age" maxlength="3" value="<?=set_value('mother_age',@$mother_age);?>">
				<small class="error">This field is required and must be numeric.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Religious Affilation</label>
				<?=form_error('mother_relaffil');?>
				<input type="text" name="mother_relaffil" other="Mother's Religious Affilation" value="<?=set_value('mother_relaffil',@$mother_religion);?>">
				<small class="error">This field is xequired.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Citizenship</label>
				<?='<br>'.form_error('mother_citizenship');?>
				<input type="text" name="mother_citizenship" other="Mother's Citizenship" value="<?=set_value('mother_citizenship',@$mother_nationality);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Educational Attainment</label>
				<?='<br>'.form_error('mother_educ');?>
				<input type="text" name="mother_educ" other="Mother's Educational Attainment" value="<?=set_value('mother_educ',@$mother_educ_attain);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Hobbies/Talent</label>
				<?='<br>'.form_error('mother_talent');?>
				<input type="text" name="mother_talent" other="Mother's Hobbies/Talent" value="<?=set_value('mother_talent',@$mother_hobby_talent);?>">
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Occupation/Position</label>
				<?='<br>'.form_error('mother_occup');?>
				<input type="text" name="mother_occup" other="Mother's Occupation/Position" value="<?=set_value('mother_occup',@$mother_occupation);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Office Address</label>
				<?='<br>'.form_error('mother_office_address');?>
				<input type="text" name="mother_office_address" other="Mother's Office Address" value="<?=set_value('mother_office_address',@$mother_office_add);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Mother's Office Telephone Number</label>
				<?='<br>'.form_error('mother_office_tel');?>
				<input type="text" name="mother_office_tel" other="Mother's Office Telephone Number" value="<?=set_value('mother_office_tel',@$mother_office_num);?>" >
				<small class = "error">Should be numeric. If none input '0'</small>
			</div>
		</div>
	</div>
	
	<div>
		<legend class="label" style="margin:10px 0;">Information about the guardian</legend>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Full Name</label>
				<?='<br>'.form_error('guardian_name');?>
				<input type="text" name="guardian_name" other="Guardian's Full Name" value="<?=set_value('guardian_name',@$guardian_name);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Relation To Child</label>
				<?='<br>'.form_error('guardian_relation');?>
				<input type="text" name="guardian_relation"other="Guardian's Relation to child" value="<?=set_value('guardian_relation',@$relationship);?>" >
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Address</label>
				<?='<br>'.form_error('guardian_address');?>
				<input type="text" name="guardian_address" other="Guardian's Address" value="<?=set_value('guardian_address',@$guardian_address);?>">
				<small class="error">This field is xequired.</small>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label class="radius secondary label">Guardian's Contact Number</label>
				<?='<br>'.form_error('guardian_contact_num');?>
				<input type="text" name="guardian_contact_num" other="Guardian's Contact Number" value="<?=set_value('guardian_contact_num',@$guardian_contact_no);?>">
				<small class="error">This field is xequired.</small>
			</div>
			<div class="large-4 columns">
				<label class="radius secondary label">Reason for Need of Guardian</label>
				<?='<br>'.form_error('guardian_reason');?>
				<input type="text" name="guardian_reason" other="Reason for Need of Guardian" value="<?=set_value('guardian_reason',@$guardian_reason);?>">
				<small class="error">This field is xequired.</small>
			</div>
		</div>
	</div>
	<!-- End of personal information -->

	<legend class="label" style="margin:0 auto;">Family History: Submit legal documents to office to validate answer.</legend>
	<hr style="border:1px solid #2BA6CB;">
	<div class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Status of Parents</label>
			<?='<br>'.form_error('parent_status');?>
			<? echo form_dropdown('parent_status',array(''=>'--select--','married'=>'married',
										   'remarried'=>'remarried',
										   'living together'=>'living together',
										   'separated'=>'separated',
										   'single parent'=>'Single Parent'),set_value('parent_status',@$parent_status),'others="Parent\'s Status" '); ?>
			<small class="error">This field is xequired.</small>							   
		</div>
		<div  class="large-4 columns">
			<label class="radius secondary label">For How Long</label>
			<?='<br>'.form_error('status_how_long');?>
			<input type="text" name="status_how_long" other="Parent's Status how long" value="<?=set_value('status_how_long',@$parent_status_how_long);?>" >
			<small class="error">This field is xequired.</small>
		</div>
		<div class="large-4 columns">
				<label class="radius secondary label">Position of Child in Family</label>
				<?='<br>'.form_error('position_of_child');?>
				<?=form_dropdown('position_of_child',$child_birth_position,set_value('position_of_child'),'other="Position of Child in the family" ');?>
				<small class="error">This field is xequired.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="label-good">(separated or single) Do parents have rights over child?</label>
			<?=form_error('right_over_child');?>
			<?=form_dropdown('right_over_child',array(''=>'--select--','yes'=>'Yes Both parents have rights','no'=>'no'),set_value('right_over_child',@$parent_rights),'other="Rights over child"')?>
		</div>
		<div class="large-4 columns">
			<label class="label-good">If NO Please specifiy who only has right.</label>
			<?=form_error('right_over_child_whom');?>
			<input type="text" name="right_over_child_whom" other="if NO Please specifiy who only has right." value="<?=set_value('right_over_child_whom',@$parents_rights_who);?>">
		</div>
	</div>
	
	<!-- siblings -->
	<!--
		<div>
			<legend class="label" style="margin:0 auto;">Other Family Members (SIBLINGS ONLY)</legend>
			<div class="btn-group">
			  <button class="btn add">+</button>
			  <button class="btn del">-</button>
			  <button class="btn clrs">clear</button>
			</div>
		<hr class="clearfix">
		
		<div class="input-fields">
			<div class="large-4 columns">
				<label class="label secondary">Firstname</label>
				<input type="text" name="sibling[firstname][]" class="quantity">
			</div>
			<div class="large-3 columns">
				<label class="label secondary">Middlename</label>
				<input type="text" name="sibling[middlename][]" class="description">
			</div>
			<div class="large-4 columns">
				<label class="label secondary">Lastname</label>
				<input type="text" name="sibling[lastname][]" class="description">
			</div>
			<div class="large-1 columns">
				<label class="label secondary">Age</label>
				<input type="text" name="sibling[age][]" class="description" pattern="[0-9]">
				<small class="error">Age must be numeric</small>
			</div>
			<hr class="clearfix">
		</div>
		
		<div id='dynapage'></div>
		
		</div>
		
		-->
	
	<div class="row">
		<div class="large-5 columns">
			<label class="radius secondary label">Is Child Adopted</label>
			<?='<br>'.form_error('is_child_adopted');?>
			<?=form_dropdown('is_child_adopted',array(''=>' -- select --','no'=>'No','yes'=>'Yes'),set_value('is_child_adopted'),'other="Is Child adopted"');?>
			<small class="error">This field is xequired.</small>
		</div>
		<div class="large-2 columns">
			<label class="radius secondary label">Age of Adoption</label>
			<?='<br>'.form_error('age_of_adoption');?>
			<input type="text" name="age_of_adoption" maxlength="3" other="If adopted age of adoption?" value="<?=set_value('age_of_adoption');?>" >
			<small class="error">Age Must be numeric</small>
		</div>
		<div class="large-5 columns">
			<label class="radius secondary label">Is the child aware he/she is adopted?</label>
			<?='<br>'.form_error('child_aware_adopted');?>
			<?=form_dropdown('child_aware_adopted',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('child_aware_adopted'),'other="If adopted is child aware he/she is adopted"');?>
		</div>
	</div>
	
	<div class="row">
		<div class="large-6 columns">
			<label class="radius secondary label">Is the mother presently pregnant?</label>
			<?='<br>'.form_error('mother_presently_pregnant');?>
			<?=form_dropdown('mother_presently_pregnant',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('mother_presently_pregnant'),'other="Mother presently pregnant" ');?>
			<small class="error">This field is xequired.</small>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">If Yes Due date</label>
			<?='<br>'.form_error('mother_pregnancy_due_date');?>
			<input type="text" name="mother_pregnancy_due_date" other="If mother presently pregnant, due date" value="<?=set_value('mother_pregnancy_due_date');?>">
		</div>
	</div>
	
	<div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Has there been any deaths in the immediate family?</label>
				<?='<br>'.form_error('family_deaths');?>
				<?=form_dropdown('family_deaths',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('family_deaths'),'other="Any family deaths?"');?>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">Relation to child</label>
				<?='<br>'.form_error('family_deaths_relation');?>
				<input type="text" name="family_deaths_relation" other="Relation to child" value="<?=set_value('family_deaths_relation');?>">
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Has there been any serious accidents in the family?</label>
				<?='<br>'.form_error('family_accidents');?>
				<?=form_dropdown('family_accidents',array(''=>'--select--','no'=>'No','yes'=>'Yes'),set_value('family_accidents'),'other="Any serious accidents in the family"');?>
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">Relation to child</label>
				<?='<br>'.form_error('family_accidents_relation');?>
				<input type="text" name="family_accidents_relation" other="Relation to child" value="<?=set_value('family_accidents_relation');?>">
			</div>
		</div>
		
		<div class="clearfix"></div>
		<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">What?:</label>
				<?='<br>'.form_error('what');?>
				<input type="text" name="what" other="what" value="<?=set_value('what');?>">
			</div>
			<div  class="large-6 columns">
				<label class="radius secondary label">When?:</label>
				<?='<br>'.form_error('when');?>
				<input type="text" name="when" other="when" value="<?=set_value('when');?>">
			</div>
		</div>
	</div>
	
	<div>
	<legend class="label" style="margin:10px 0;">Other Household Members (Relatives,Helpers, etc..)</legend>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers1');?>
			<input type="text" name="hhmembers1" other="Other house hold members name" value="<?=set_value('hhmembers1');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage1');?>
			<input type="text" name="hhmembersage1" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage1');?>" pattern="[0-9]">
			<small class="error">Age must be numeric</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers2');?>
			<input type="text" name="hhmembers2" other="Other house hold members name" value="<?=set_value('hhmembers2');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage2');?>
			<input type="text" name="hhmembersage2" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage2');?>" pattern="[0-9]">
			<small class="error">Age must be numeric</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers3');?>
			<input type="text" name="hhmembers3" other="Other house hold members name" value="<?=set_value('hhmembers3');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage3');?>
			<input type="text" name="hhmembersage3" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage3');?>">
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers4');?>
			<input type="text" name="hhmembers4" other="Other house hold members name" value="<?=set_value('hhmembers4');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage4');?>
			<input type="text" name="hhmembersage4" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage4');?>" pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers5');?>
			<input type="text" name="hhmembers5" other="Other house hold members name" value="<?=set_value('hhmembers5');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage5');?>
			<input type="text" name="hhmembersage5"other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage5');?>"pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	<div class="row">
		<div class="large-8 columns">
			<label class="radius secondary label">Name</label>
			<?='<br>'.form_error('hhmembers6');?>
			<input type="text" name="hhmembers6" other="Other house hold members name" value="<?=set_value('hhmembers6');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Age</label>
			<?='<br>'.form_error('hhmembersage6');?>
			<input type="text" name="hhmembersage6" other="Other house hold members age" maxlength="3" value="<?=set_value('hhmembersage6');?>" pattern="[0-9]">
			<small class="error">Age must be numeric.</small>
		</div>
	</div>
	</div>
	<div>
		<label class="radius secondary label">Languages Spoken at home?</label>
		<?='<br>'.form_error('language_at_home');?>
		<input type="text" name="language_at_home"other="Languages spoken at home" value="<?=set_value('language_at_home');?>">
		<small class="error">This field is xequired.</small>
	</div>
	
	<div class="row">
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Activites Family Engages in?</label>
			<?='<br>'.form_error('family_activities');?>
			<textarea name="family_activities" other="Activites Family Engages in?"><?=set_value('family_activities');?></textarea>
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">How frequent?</label>
			<?='<br>'.form_error('family_activities_frequent');?>
			<textarea name="family_activities_frequent" other="How frequent?"><?=set_value('family_activities_frequent');?></textarea>
		</div>
	</div>
	</div>
		
	<!-- <div>
		<legend class="label" style="margin:15px 0;">How much time does the child spend in</legend>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Watching TV or videos in a day?</label>
				<?='<br>'.form_error('tv_time');?>
				<input type="text" name="tv_time" other="Child spend watching TV" value="<?=set_value('tv_time');?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('tv_whom');?>
				<input type="text" name="tv_whom" other="Child spend watching TV with whom" value="<?=set_value('tv_whom');?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Listening to radio in a day?</label>
				<?='<br>'.form_error('radio_time');?>
				<input type="text" name="radio_time" other="Listening to radio in a day?" value="<?=set_value('radio_time');?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('radio_whom');?>
				<input type="text" name="radio_whom" other="Listening to radio in a day? with whom" value="<?=set_value('radio_whom');?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Playing computer games in a day?</label>
				<?='<br>'.form_error('computergames_time');?>
				<input type="text" name="computergames_time" other="Playing computer games in a day?" value="<?=set_value('');?>">
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">With Whom</label>
				<?='<br>'.form_error('computergames_whom');?>
				<input type="text" name="computergames_whom"  other="Playing computer games in a day? with whom"  value="<?=set_value('computergames_whom');?>">
			</div>
		</div>
	</div>
		
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Does the child have any Responsibilities/Duties in home?</label>
				<?='<br>'.form_error('child_responsibilities');?>
				<input type="text" name="child_responsibilities"  other="Child have any responsibilities at home"  value="<?=set_value('child_responsibilities');?>">
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">What are his/her responsibilities?</label>
					<?='<br>'.form_error('child_responsibilities_what');?>
					<input type="text" name="child_responsibilities_what" other="What are his/her responsibilities" value="<?=set_value('child_responsibilities_what');?>">
			</div>
		</div>
		<div>
			<div class="large-6 columns">
				<label class="radius secondary label">Does child have a play group?</label>
				<?='<br>'.form_error('child_play_group');?>
				<?=form_dropdown('child_play_group',array(''=>'-- select --','no'=>'No','yes'=>'Yes'),set_value('child_play_group'),'other="Child has playgroup?"');?>
			</div>
			<div class="large-6 columns">
					<label class="radius secondary label">How frequent does child play with group</label>
					<?='<br>'.form_error('child_play_group_frequent');?>
					<input type="text" name="child_play_group_frequent" other="How frequent does child play with playgroup" value="<?=set_value('child_play_group_frequent');?>">
			</div>
		</div>
		<div class="clearfix"></div>
		<div>
			<label class="radius secondary label">What are the other activites/interest of your child?</label>
			<?='<br>'.form_error('other_interest');?>
			<textarea name="other_interest" other="What are the other activites/interest of your child?"><?=set_value('other_interest');?></textarea>
		</div>	 -->

<!-- 
==============================================================
ADDITIONAL FIELDS IF Preschool
==============================================================


<?if(isset($preschool)):?>
	<?$this->load->view('enrollment/_additional_preschool');?>
<?endif;?>-->
<!-- 
==============================================================
ADDITIONAL FIELDS IF Primary
==============================================================


<?if(isset($primary)):?>
	<?$this->load->view('enrollment/_additional_primary');?>
<?endif;?>-->

<!-- 
==============================================================
ADDITIONAL FIELDS IF Grade School OR Secondary
==============================================================

<?if(isset($gradeschool)):?>
	<?$this->load->view('enrollment/_additional_secondary');?>
<?endif;?>-->


<!-- 
==============================================================
ADDITIONAL FIELDS IF High School or Intermediate
==============================================================

<?if(isset($highschool)):?>
	<?$this->load->view('enrollment/_additional_highschool');?>
<?endif;?>-->


<hr class="clearfix">
<div class="form-values">
	<input type="hidden" name="sdf_loc" value="<?=$token;?>">
	<input type="submit" name="fillup_profile" id="submit_fillup_profile" value="Finish Enrollment" class="btn btn-success">
	<!--<input type="submit" name="submit_save_later" id="submit_fillup_profile" value="Save and Continue Later" class="btn btn-primary"> -->
</div>
<?php echo form_close(); ?>
</div>