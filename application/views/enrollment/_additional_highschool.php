
<legend class="label" style="margin:0 auto;">Student Additional Information</legend>
<hr style="border:1px solid #2BA6CB;">
<div class="row">
<div class="large-12 columns panel">
	<h6>FAMILY'S FAVORITE PASTIME (Please Specify all that applies) :</h1>
</div>
<div  class="row">
	<div class="large-4 columns">
		<p class="label radius secondary ">Watching T.V./movies/videos</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_watch');?>
		<input type="text" other="Watching TV/Movies/Videos" name="hs_watch" value="<?=set_value('hs_watch',isset($sse)? $sse['intermediate_watch']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
	<div class="large-4 columns">
		<p class="label radius secondary ">Playing Board Games</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_boardgames');?>
		<input type="text" other="Playing Board Games" name="hs_boardgames" value="<?=set_value('hs_boardgames',isset($sse)? $sse['intermediate_boardgames']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
	<div class="large-4 columns">
		<p class="label radius secondary ">Computer</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_comp');?>
		<input type="text" other="Computer" name="hs_comp" value="<?=set_value('hs_comp',isset($sse)? $sse['intermediate_comp']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
	<div class="large-4 columns">
		<p class="label radius secondary ">Reading</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_reading');?>
		<input type="text" other="Reading" name="hs_reading" value="<?=set_value('hs_reading',isset($sse)? $sse['intermediate_reading']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
	<div class="large-4 columns">
		<p class="label radius secondary ">Others</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_other_pt');?>
		<input type="text" other="Others" name="hs_other_pt" value="<?=set_value('hs_other_pt',isset($sse)? $sse['intermediate_other_pt']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
	<div class="large-4 columns">
		<p class="label-good">How much time does your child spend in his/her study period?</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_spend_study');?>
		<input type="text" other="Time spent in his/her Study Period" name="hs_spend_study" value="<?=set_value('hs_spend_study',isset($sse)? $sse['intermediate_spend_study']:NULL);?>" >
		<small class="error">This field is required.</small>
	</div>
</div>

<div  class="row">
		<div class="large-4 columns">
			<p class="label-good">Do you allow your child to go home independently after class? Why?</p>
		</div>
		<div class="large-8 columns">
			<?=form_error('al_tghome');?>
			<textarea name="al_tghome" other="Do you allow your child to go home independently after class?" ><?=set_value('al_tghome',isset($sse)? $sse['go_home_independently']:NULL);?></textarea>
			<small class="error">This field is required.</small>
		</div>
	</div>

<legend class="label" style="margin:0 auto;">FOR FEMALE CHILDREN</legend>
<hr style="border:1px solid #2BA6CB;">
<div  class="row">
	<div class="large-4 columns">
		<p class="label-good">Has your child had her first menstruation?</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_first_mens');?>
		<?=form_dropdown('hs_first_mens',array(''=>'--select--','yes'=>'Yes, child had first menstruation','no'=>'No, child never had menstruation'),set_value('hs_first_mens',isset($sse)? $sse['intermediate_first_mens']:NULL),'other="Child First Menstruation"');?>
	</div>
</div>
<div  class="row">
	<div class="large-4 columns">
		<p class="label-good">What normally signals or accompanies her monthly period?</p>
	</div>
	<div class="large-8 columns">
		<?=form_error('hs_signal_mens');?>
		<input type="text" other="What normally signals or accompanies her monthly period?" name="hs_signal_mens" value="<?=set_value('hs_signal_mens',isset($sse)? $sse['intermediate_signal_mens']:NULL);?>">
	</div>
</div>
</div>