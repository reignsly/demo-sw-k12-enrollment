<?
	$email = $saved_input ? $saved_input['email'] : NULL;
	$code = $saved_input ? $saved_input['security_code'] : NULL;
?>
<div class="large-2 columns">&nbsp;</div>
<div class="large-8 columns enrollment-menu content-views">
<?=$system_message;?>
<div class="row" style="padding:20px;">
	<div class="panel">
		Please fill the form below to resume to your saved enrollment. If you 
		have enrolled more than one child, a Unique Security Code will be sent for each child. 
	</div>
	<h5>Resume Saved Enrollment:</h5>
	<form action="<?=site_url('enrollment/resume_enrollment');?>" method="POST" autocomplete="off">
		<label class="label">Please Enter the Email used for registration.</label>
		<input type="email" name="resume[email]" value="<?=set_value('email',$email);?>">
		<label class="label">Please Enter Security Code Sent to you via Email.</label>
		<input type="text" name="resume[security_code]" maxlength="8" value="<?=set_value('email',$code);?>">
		
		<div class="large-6 columns">
			<label class="label">Please verify you are human</label>
			<input type="text" name="captcha_code" placeholder="Enter Code found at the right" maxlength="10">
		</div>
		<div class="large-6 columns">
			<?=$question;?>
		</div>

		<input type="hidden" name="skd_kkl" value="<?=$token;?>">
		<input type="submit" name="continue_enrollment" value="Continue Enrollment" class="btn btn-success">
		<a href="<?=site_url('enrollment');?>" class="btn btn-mini btn-warning">Go Back to main menu</a>
	</form>
</div>
</div>
<div class="large-2 columns">&nbsp;</div>