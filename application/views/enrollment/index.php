<style>
	div.enrollment-menu
	{
		padding:20px;
		border:2px dashed #c0c0c0;
	}
</style>

<script>
$(function() {
	$('input:radio').change(
		function(){
			var id = $(this).attr('id');
			$('fieldset div.option').removeClass('alert alert-success').addClass('alert alert-info');
			$(this).parent('div').removeClass('alert-info').addClass('alert alert-success');
		}
); 
});
</script>
<div class="row">
	<div class="large-2 columns">&nbsp;</div>
	<div class="large-8 columns enrollment-menu content-views">
	<?php if(isset($system_message)) echo $system_message;?>
	<div style="padding:20px;margin-bottom:10px;">
		<div class="btn-group">
			<a href="<?=site_url('confirm/resend');?>" class="btn btn-small btn-warning">Resend Confirmation Code</a>
			<a href="<?=site_url('enrollment/resume_enrollment');?>" class="btn btn-small btn-success">Resume Enrollment</a>
		</div>	
	</div>
	<?echo form_open('enrollment/enrollment_procedure','class="custom" data-abide')?>
		<div style="border:1px solid #c0c0c0;padding:20px;margin-bottom:10px;">
		<div class="row">
			<div class="large-12 columns">
				<p style="font-weight:bold;">Please Select Type Of Enrollment.</p>
			</div>
		</div>
		<p class="small-info">
			For the Current School Year <?=$this->schoolyear_date;?> all student New or Old are coming in as new enrollee.
			Please continue. New Enrollee is already Selected For you.
		</p>
		<div class="option alert alert-info">
			<input type="radio" id="new_enrollee" name="level_option" value="new" required checked/>&nbsp;<span class="label">New Enrollee</span><i class="option">&nbsp;&nbsp;</i>
			<small class="error">Please select enrollment method.</small>
		</div>
		<!--
		<div  class="option alert alert-info">
			<input type="radio" id="old_enrollee" name="level_option" value="old" required>&nbsp;<span class="label">Old Enrollee</span><i class="option">&nbsp;&nbsp;</i>
		</div>
		-->
		 </div>
		
		<div style="border:1px solid #c0c0c0;padding:20px;">
		<div class="row">
			<div class="large-6 columns">
			  <label class="label">Enter Captcha found on the right</label>
			  <input type="text" name="captcha_answer" autocomplete="off" style="border:#ccc 2px solid;" maxlength="5" required pattern="[a-zA-Z0-9]+">
			  <small class="error">Captcha is required and must be alphanumeric.</small>
			</div>
			<div  class="large-6 columns" >
				<?=$question;?>
			</div>
		</div>
		 </div>
		 <div  class="option">
		  <input type="hidden" name="sdj" value="<?=$token;?>">
		  <input type="hidden" name="enrollment_type" value="true">
		  <input type="submit" class="btn btn-primary" name="enrollment_type" value="Continue">
		 </div>
		 <?echo form_close()?>
	</div>
	<div class="large-2 columns">&nbsp;</div>
</div>
	