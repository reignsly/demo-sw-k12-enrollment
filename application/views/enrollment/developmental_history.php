<?php
$this->load->view('enrollment/enrollment_modal');
$genderAttrib = array('' => 'Select Gender...', 'male' => 'Male', 'female' => 'Female' );
$child_birth_position = array(''=>'','first'=>'First','Second'=>'Second','third'=>'Third','fourth'=>'Fourth','fifth'=>'Fifth','sixth'=>'Sixth','youngest'=>'youngest','only'=>'Only');
?>
<div class="row">
<div class="large-12 columns enrollment-menu  content-views">
<? echo $system_message;?>
<div class="alert-box">REGISTRATION FORM Part 2: Developmental History of Child And Childhood Experiences</div>
<?$this->load->view('enrollment/notice')?>
<?echo form_open('developmental','class="custom" id="check-form-submit" data-abide  autocomplete="off"');?>
	<legend class="label" style="margin:0 auto;">Developmental History Of Child</legend>		
	<div class="row">
		<div class="large-8 small-12 columns">
			<label class="radius secondary label">Length of Pregnancy</label>
			<?=form_error('length_of_preg');?>
			<input type="text" name="length_of_preg" value="<?=set_value('length_of_preg');?>" >
			
		</div>
		<div class="large-4 small-12 columns">
			<label class="radius secondary label">Form of Delivery</label>
			<?=form_error('form_of_del');?>
			<input type="text" name="form_of_del" value="<?=set_value('form_of_del');?>" >
			
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
		<label class="radius secondary label">Complications before? during? after? delivery?</label>
		<?=form_error('complications');?>
		<input type="text" name="complications" value="<?=set_value('complications');?>" >
		
		</div>
	</div>
	<div class="row">
		<div class="large-6 columns">
			<label class="radius secondary label">What time does the child go to bed?</label>
			<?=form_error('go_to_bed');?>
			<input type="text" name="go_to_bed" value="<?=set_value('go_to_bed');?>" >
			
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Get up from bed?</label>
			<?=form_error('get_up_bed');?>
			<input type="text" name="get_up_bed" value="<?=set_value('get_up_bed');?>" >
			
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<label class="radius secondary label">Does the child have any sleeping disturbance? if yes what?</label>
			<?=form_error('sleeping_dist');?>
			<input type="text" name="sleeping_dist" value="<?=set_value('sleeping_dist');?>" >
		</div>
	</div>
	
	<div class="row">
			<div class="large-6 columns">
				<label class="radius secondary label">Does the child have own room?</label>
				<?=form_error('own_room');?>
				<?=form_dropdown('own_room',array(''=>'-- select --','yes'=>'Yes','no'=>'No'),set_value('own_room'),'');?>
				
			</div>
			<div class="large-6 columns">
				<label class="radius secondary label">If not, shares with whom?</label>
				<?=form_error('room_shares');?>
				<input type="text" name="room_shares" value="<?=set_value('room_shares');?>">
			</div>
	</div>
	<div class="row">
		<div class="large-4 columns">
			<label class="radius secondary label">Does the child bed wet?</label>
			<?=form_error('wet_bed');?>
			<?=form_dropdown('wet_bed',array(''=>'-- select --','yes'=>'Yes','no'=>'No'),set_value('wet_bed'),'');?>
			
		</div>
		<div class="clearfix"></div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any observable difficulties or defects in your child's growth?</legend>
		<div class="large-4 columns">
			<label class="radius secondary label">Specify:</label>
			<?=form_error('observable_difficulties');?>
			<input type="text"  name="observable_difficulties" value="<?=set_value('observable_difficulties');?>" >
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Since When?</label>
			<?=form_error('observable_difficulties_since_when');?>
			<input type="text"  name="observable_difficulties_since_when" value="<?=set_value('since_when');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Action Taken:</label>
			<?=form_error('observable_difficulties_action_taken');?>
			<input type="text"  name="observable_difficulties_action_taken" value="<?=set_value('observable_difficulties_action_taken');?>">
		</div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any speech problems?</legend>
		<div class="large-4 columns">
			<label class="radius secondary label">Specify:</label>
			<?=form_error('speech_problems');?>
			<input type="text"  name="speech_problems" value="<?=set_value('speech_problems');?>" >
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Since When?</label>
			<?=form_error('speech_problems_since_when');?>
			<input type="text"  name="speech_problems_since_when" value="<?=set_value('speech_problems_since_when');?>" >
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Action Taken:</label>
			<?=form_error('speech_problems_actions_taken');?>
			<input type="text"  name="speech_problems_actions_taken" value="<?=set_value('speech_problems_actions_taken');?>">
		</div>
	</div>
	<hr class="clearfix">
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any hearing problems?</legend>
		<div class="large-12 columns">
			<label class="radius secondary label">When was the hearing of Child last checked?</label>
			<?=form_error('hearing_last_checked');?>
			<input type="text"  name="hearing_last_checked" value="<?=set_value('hearing_last_checked');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Specify:</label>
			<?=form_error('hearing_problems');?>
			<input type="text"  name="hearing_problems" value="<?=set_value('hearing_problems');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Since When?</label>
			<?=form_error('hearing_problems_since_when');?>
			<input type="text"  name="hearing_problems_since_when" value="<?=set_value('hearing_problems_since_when');?>">
		</div>
		<div class="large-4 columns">
			<label class="radius secondary label">Action Taken:</label>
			<?=form_error('hearing_problems_actions_taken');?>
			<input type="text"  name="hearing_problems_actions_taken" value="<?=set_value('hearing_problems_actions_taken');?>">
		</div>
	</div>
	<hr class="clearfix">		
	<div class="row">
		<legend class="radius label" style="margin:0 auto;">Are there any sight problems?</legend>
		<div class="large-6 columns">
			<label class="radius secondary label" >When was the vision of Child last checked?</label>
			<?=form_error('vision_last_checked');?>
			<input type="text"  name="vision_last_checked" value="<?=set_value('vision_last_checked');?>">
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Specify:</label>
			<?=form_error('vision_specify');?>
			<input type="text"  name="vision_specify" value="<?=set_value('vision_specify');?>">
		</div>
	</div>
		
		<div>
			<label class="radius secondary label-good">Are there any information that we should be aware of that may hinder the child's learning process or general development?</label>
			<?=form_error('hinder_childs_learning');?>
			<textarea name="hinder_childs_learning" ><?=set_value('hinder_childs_learning');?></textarea>
			
		</div>

<hr class="clearfix">	
	<legend class="label" style="margin:0 auto;">Childhood Experiences</legend>
	<br>
	<div>
		<label class="radius secondary label">Has the child encountered any traumatic experiences? if yes specify</label>
		<?=form_error('traumatic_experiences');?>
		<textarea name="traumatic_experiences"><?=set_value('traumatic_experiences');?></textarea>
	</div>
	<div>
		<div class="large-6 columns">
			<label class="radius secondary label">Age</label>
			<?=form_error('age_trauma');?>
			<input type="text" name="age_trauma" value="<?=set_value('age_trauma');?>">
		</div>
		<div class="large-6 columns">
			<label class="radius secondary label">Reaction</label>
			<?=form_error('trauma_reaction');?>
			<textarea name="trauma_reaction" ><?=set_value('trauma_reaction');?></textarea>
		</div>
	</div>
	
	<div>
		<label class="radius secondary label">How has it affected the child?</label>
		<?=form_error('how_trauma_affected_child');?>
		<textarea name="how_trauma_affected_child" ><?=set_value('how_trauma_affected_child');?></textarea>
	</div>
	
	<div>
		<label class="radius secondary label">does the child have any special fears?if yes what?</label>
		<?=form_error('special_fears');?>
		<textarea name="special_fears" ><?=set_value('special_fears');?></textarea>
	</div>
	<div>
		<label class="radius secondary label">Since when? What has triggered the fear?</label>
		<?=form_error('when_what_triggered_fear');?>
		<textarea name="when_what_triggered_fear"><?=set_value('when_what_triggered_fear');?></textarea>
	</div>
	<div>
		<label class="radius secondary label">How do you handle it?</label>
		<?=form_error('how_do_you_handle');?>
		<textarea name="how_do_you_handle"><?=set_value('how_do_you_handle');?></textarea>
	</div>

	
<div>
	<input type="hidden" name="spm_sdf" value="<?=$token;?>">
	<input type="hidden" name="fillup_developmental" value="true">
	<input type="submit" name="fillup_developmental" value="Continue to Part 3: Health History of Child And Discipline" class="btn btn-primary">
</div>
<?php echo form_close(); ?>

</div>
</div>