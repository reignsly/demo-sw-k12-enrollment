<div style="border:3px solid #c0c0c0;padding:10px;">
<h1 style="text-align:center;"><?=ucwords($school_name);?></h1>
<hr>
<p>Enrollment : Resend Confirmation Link</p>
<p>Dear <strong><?=ucwords($name);?></strong></p>
<p>Greetings from the management <strong><?=ucwords($school_name);?></strong></p>
<p style="text-indent:15px;">
Here is your new confirmation link, be advised that the confirmation link will expire after 30 minutes. Please click the button below to verify your enrollment.<br>
Thank you.
</p>
<br>
<div style="text-align:center;margin:5px;">
</h1><a href="<?=$link;?>" style="color:#fff;border:1px solid #145F0C;padding:10px;font-size:14px;font-weight:bold;background:#209812;">Click here to confirm Enrollment.</a>
</div>
<br>
<hr>
<br>
<div>
	<p><strong>Notice:</strong></p>
	<p>
	The Management:<br>
	<span style="font-weight:bold;font-size:14px;"><?=ucwords($school_name);?></span></p>
	<p style="text-indent:15px;">This email and any files transmitted with it are confidential and intended solely for the use of
	the individual or entity to whom they are addressed. If you have received this email in error
	please notify the system manager. This message contains confidential information and is
	intended only for the individual named. If you are not the named addressee you should not
	disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if
	you have received this e-mail by mistake and delete this e-mail from your system. If you are not
	the intended recipient you are notified that disclosing, copying, distributing or taking any action
	in reliance on the contents of this information is strictly prohibited.
	</p>
	<p>
	</p>
</div>
<br><br><br>
<p>This is a System Generated Message. Do Not Reply.</p>
</div>