<div style="border:3px solid #c0c0c0;padding:10px;">
<h1 style="text-align:center;"><?=ucwords($school_name);?></h1>
<hr>
<p>Dear <strong><?=ucwords($name);?></strong></p>
<p>Greetings from the management <strong><?=ucwords($school_name);?></strong></p>
<p style="text-indent:15px;">
Thank you for verifying your enrollment <br>
Please be advised that the slots are limited and we work on a first come- first served basis <br>
You are not considered enrolled unless
<ul>
	<li>The necessary charges are paid to the <?=$acc;?> Cashier</li>
	<li>Required documents are submitted to the <?=$acc;?> Registrar/Cashier</li>
</ul>
<br>
	We look forward to having you as a member of the <?=$acc;?> family.
</p>
<hr>
<br>
<div>
	<p><strong>Notice:</strong></p>
	<p>
	The Management:<br>
	<span style="font-weight:bold;font-size:14px;"><?=ucwords($school_name);?></span></p>
	<p style="text-indent:15px;">This email and any files transmitted with it are confidential and intended solely for the use of
	the individual or entity to whom they are addressed. If you have received this email in error
	please notify the system manager. This message contains confidential information and is
	intended only for the individual named. If you are not the named addressee you should not
	disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if
	you have received this e-mail by mistake and delete this e-mail from your system. If you are not
	the intended recipient you are notified that disclosing, copying, distributing or taking any action
	in reliance on the contents of this information is strictly prohibited.
	</p>
	<p>
	</p>
</div>
<br><br><br>
<p>This is a System Generated Message. Do Not Reply.</p>
</div>