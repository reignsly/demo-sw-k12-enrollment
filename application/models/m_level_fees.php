<?php
	
	
class M_level_fees Extends CI_Model
{
	private $__table = 'level_fees';
	private $_ftable = 'fees';
	private $_ltable = 'levels';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function check_level_fee_isset()
	{
		
	}
	
	public function get_grade_level($id=FALSE)
	{
		if($id == FALSE){
			$query = $this->db->select(array('level_id','level_desc','level_code'))->get($this->_ltable);
		}else{
			$query = $this->db->select(array('level_id','level_desc'))->where('level_id',$id)->get($this->_ltable);
		}
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_all_fees()
	{
		$query = $this->db->select(array('fee_id','fee_name','fee_desc','fee_rate'))->get($this->_ftable);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_sel_fees($id)
	{
		
		$sql = 
			"SELECT fees.fee_id,
					fees.fee_name,
					fees.fee_desc,
					fees.fee_rate,
					fees.is_tuition_fee
			FROM level_fees lvl_fee, fees fees,levels lvls 
			WHERE lvl_fee.lf_level_id = lvls.level_id 
			AND   lvl_fee.lf_fee_id = fees.fee_id
			AND   lvls.level_id = ?
			AND   lvl_fee.lf_level_id = ?";
		
		$query = $this->db->query($sql,array($id,$id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_tuition_fees_set_for_current_level($id)
	{
	
		$sql = "SELECT fees.fee_id,fees.fee_name,fees.fee_desc,fees.fee_rate
				FROM fees
				WHERE fees.grade_level = ? 
				AND fee_stat = 'active' 
				AND is_tuition_fee = 1;
				";	
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_active_registration_fee($id)
	{
		$sql = "SELECT fees.fee_id,fees.fee_name,fees.fee_desc,fees.fee_rate
				FROM fees
				where fee_stat = 'active' 
				AND is_registration_fee = 1 
				AND fees.grade_level = ? ";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function update_grade_level_fees($data,$id)
	{
		if($this->db->where('lf_level_id',$id)->delete($this->__table)){
			if($this->db->insert_batch($this->__table, $data)){
				return array('status'=>'true');
			}else{
				return array('status'=>'false');
			}
		}else{
			return array('status'=>'nodelete');
		}
	}
}