<?php

class M_gperiods Extends CI_Model
{
	private $__table = 'grading_periods';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_gperiods($id = FALSE)
	{
		$this->load->database();
		if($id == FALSE){
			$data = $this->db->get($this->__table);
		}else{
			$data = $this->db->where('gp_id',$id)->get($this->__table);	
		}
		return $data->num_rows() >= 1 ? $data->result() : FALSE;
	}
	
	public function fetch_gperiods($limit,$start)
	{
        $this->db->limit($limit, $start);
        $query = $this->db->select(array('gp_id','gp_code','gp_desc','gp_stat','is_set'))->get($this->__table);
		return $query->num_rows() >= 1 ? $query->result() : FALSE;		
	}
	
	public function count_gperiods()
	{
		return $this->db->count_all($this->__table);
	}
	
	public function update_gperiods($input,$id)
	{
		if($this->verify_data($input,2) == FALSE)
		{
			$input['gp_updated'] = NOW;
			$this->db->set($input)->where('gp_id',$id)->update($this->__table);
			return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');			
		}else{
			return array('status'=>'exist');
		}
	}
	
	
	public function add_gperiods($input)
	{
		if($this->verify_data($input,2) == FALSE)
		{
			$input['gp_created'] = NOW;
			$this->db->insert($this->__table,$input);
			return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');			
		}else{
			return array('status'=>'exist');
		}
		
	}
	
	public function destroy($id)
	{
		$data['gp_stat'] = 'inactive';
		$data['gp_updated'] = NOW;
		$this->db->set($data)->where('gp_id',$id)->update($this->__table);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}

	public function set($id,$current_id)
	{
		$this->db->set('is_set','')->where('gp_id',$current_id)->update($this->__table);
		if($this->db->affected_rows() > 0)
		{
			$data['is_set'] = 'yes';
			$data['gp_updated'] = NOW;
			$this->db->set($data)->where('gp_id',$id)->update($this->__table);
			return $this->db->affected_rows() > 0 ? TRUE : FALSE;
		}else{
			return FALSE;
		}

	}
	
	public function undestroy($id)
	{
		$data['gp_stat'] = 'active';
		$data['gp_updated'] = NOW;
		$this->db->set($data)->where('gp_id',$id)->update($this->__table);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
	
	public function get_id_of_current_gperiod(){
		$query = $this->db->where('is_set','yes')->select('gp_id')->get($this->__table);
		if($query->num_rows () > 0 ){
			$row = $query->row();
			return $row->gp_id;
		}
	}
	
	public function get_current_gperiod()
	{
		$query = $this->db->where('is_set','yes')->get($this->__table);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_current_gperiods_value($data)
	{
		$query = $this->db->select($data)->where('is_set','yes')->get($this->__table);
		if($query->num_rows() > 0 )
		{
			$row = $query->row();
			return $row->$data;
		}
	}
	
	
	public function get_specific_coloumn($data)
	{	
		$query = $this->db->select($data)->get($this->__table);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	/* verify_data
	 * @param array
	 * @param int
	*  verifies if data entered by user is existing already in the database
	*  $strict level 1,2
	*  level 1 = if one field has existing data in database returns true else false
	*  level 2 = if all field has existing data in database returns true else false
	*  12/11/2012
	*/
	public function verify_data($data,$strict_level)
	{
			if($strict_level == 1)
			{
				$query = $this->db->or_where($data)->get($this->__table);
			}elseif($strict_level == 2){
				$query = $this->db->where($data)->get($this->__table);
			}
			return $query->num_rows() > 0 ? TRUE : FALSE;
	}
}