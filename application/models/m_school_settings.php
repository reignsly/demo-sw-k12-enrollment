<?php
class M_school_settings extends CI_Model {
	
	private $__table = 'school_settings';
	
	public function get_all_school_setting($id='')
	{
		$query = $this->db->limit(1)->get($this->__table);
		return $query->num_rows() >= 1 ? $query->result() : FALSE;
	}
	
	public function check_online_status()
	{
		$sql = "SELECT online_enrollment as status
				FROM system_settings
				LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows() >=1 ? $query->row() : FALSE;
	}
	
	public function check_email_verification()
	{
		$sql = $this->db->select('enrollment_verify_email')->limit(1)->get('system_settings');
		if($sql->num_rows() >=1)
		{
			if($sql->row()->enrollment_verify_email == 1)
			{
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	public function get_custom_message($type)
	{
		$sql = 'SELECT id,type,message 
				FROM system_custom_message scm
				WHERE type = "'.$type.'"
				LIMIT 1';
				
		$q = $this->db->query($sql);
		
		return $q->num_rows() >= 1 ? $q->row() : FALSE;
	}
}
?>