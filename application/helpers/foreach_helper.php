<?php

function all_fees($data)
{
	if(!empty($data))
	{
		foreach($data as $key => $value){
			$all_fees[$value->fee_id]['name'] = $value->fee_name;
			$all_fees[$value->fee_id]['rate'] = $value->fee_rate;
			$all_fees[$value->fee_id]['desc'] = $value->fee_desc;
			$all_fees[$value->fee_id]['id'] = $value->fee_id;
		}
		return $all_fees;
	}else{
		return FALSE;
	}
}

function selected_fees($data)
{
	if(!empty($data))
	{
		foreach($data as $key => $value){
				$selected_fees[$value->fee_id]['name'] = $value->fee_name;
				$selected_fees[$value->fee_id]['rate'] = $value->fee_rate;
				$selected_fees[$value->fee_id]['desc'] = $value->fee_desc;
				$selected_fees[$value->fee_id]['id'] = $value->fee_id;
		}
		return $selected_fees;
	}else{
		return FALSE;
	}
}

function available_fees($all_fees,$selected_fees)
{
	if(!empty($selected_fees))
	{
			
	 foreach($all_fees as $key => $value){
		$allfees[$value['id']]['rate'] = $value['rate'];
		$allfees[$value['id']]['desc'] = $value['desc'];
		$allfees[$value['id']]['name'] = $value['name'];
		$allfees[$value['id']]['id'] = $value['id'];
	 }
	 foreach($selected_fees as $key => $value)
	 {
		$selfees[$value['id']]['rate'] = $value['rate'];
		$selfees[$value['id']]['desc'] = $value['desc'];
		$selfees[$value['id']]['name'] = $value['name'];
		$selfees[$value['id']]['id'] = $value['id'];		 
	}
	
	foreach($all_fees as $key => $value){
			if(!array_key_exists($key,$selfees)){
				$data[] = $value;
			}
	}	
	return $data;	
	}else
	{
		return $all_fees;
	}	
		
}