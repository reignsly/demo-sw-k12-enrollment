<?php

include APPPATH.'config/includes/form_validation/base'.EXT;
include APPPATH.'config/includes/form_validation/highschool'.EXT;
include APPPATH.'config/includes/form_validation/preschool'.EXT;
include APPPATH.'config/includes/form_validation/primary'.EXT;
include APPPATH.'config/includes/form_validation/secondary'.EXT;


$config = [
	'preschool' => array_merge($preschool,$developmentalhistory,$healthhistory),
	'primary' => array_merge($primary,$developmentalhistory,$healthhistory),
	'secondary' => array_merge($secondary,$developmentalhistory,$healthhistory),
	'highschool' => array_merge($highschool,$developmentalhistory,$healthhistory),
	'none'=>'',
];






