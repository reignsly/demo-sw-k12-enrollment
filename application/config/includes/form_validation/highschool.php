<?php

$highschool = [
		array(
		'field' => 'child_fname',
		'label' => 'Child First name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_lname',
		'label' => 'Child Last Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_mname',
		'label' => 'Childs Middle Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'child_nickname',
		'label' => 'Childs Nickname',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'b_month',
		'label' => 'Birth month',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
		array(
		'field' => 'b_day',
		'label' => 'Day Of Birth',
		'rules' => 'required|datechecker_2|trim|htmlspecialchars|strip_tags'
	),  
		array(
		'field' => 'b_year',
		'label' => 'Year of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'gender',
		'label' => 'Childs Gender',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_placeofbirth',
		'label' => 'Child\'s Place of Birth',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_nationality',
		'label' => 'Child\'s Nationality',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_religous',
		'label' => 'Religous Affilation',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_city_address',
		'label' => 'Childs City Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_telno',
		'label' => 'Contact Number',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'email',
		'label' => 'Email Address',
		'rules' => 'required|valid_email|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_adresshowlong',
		'label' => 'How Long Has Child Been Living',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_last_attended',
		'label' => 'School Last Attended',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_level_completed',
		'label' => 'School Level Completed',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'school_address',
		'label' => 'School Address',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_name',
		'label' => 'Father\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_age',
		'label' => 'Father\'s Age',
		'rules' => 'required|numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'father_relaffil',
		'label' => 'Father\'s Religous Affilation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),	
	array(
		'field' => 'father_citizenship',
		'label' => 'Father\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_educ',
		'label' => 'Father\'s Educational Attainment',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'father_talent',
		'label' => 'Father\'s Hobies and Talent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_occup',
		'label' => 'Father\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_address',
		'label' => 'Father\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'father_office_tel',
		'label' => 'Father\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_name',
		'label' => 'Mother\'s Name',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_age',
		'label' => 'Mother\'s Age',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	), 
	array(
		'field' => 'mother_relaffil',
		'label' => 'Mother\'s Religous Affilation',
		'rules' => 'text_all|required|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_citizenship',
		'label' => 'Mother\'s Citizenship',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_educ',
		'label' => 'Mother\'s Educational Attainment',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_talent',
		'label' => 'Mother\'s Hobbies and Talent',
		'rules' => 'trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_occup',
		'label' => 'Mother\'s Occupation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_office_address',
		'label' => 'Mother\'s Office Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_office_tel',
		'label' => 'Mother\'s Office Tel',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_name',
		'label' => 'Guardians Name',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_relation',
		'label' => 'Guardian Relation',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_address',
		'label' => 'Guardian\'s Address',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'guardian_contact_num',
		'label' => 'Guardian\'s Contact Number',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'guardian_reason',
		'label' => 'Reason For Having Guardian',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'parent_status',
		'label' => 'Parent\'s Marriage Status',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'status_how_long',
		'label' => 'For how long',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'right_over_child',
		'label' => 'Right over Child',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'position_of_child',
		'label' => 'Child\'s Position',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'is_child_adopted',
		'label' => 'Is Child Adopted',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'age_of_adoption',
		'label' => 'Age Of Adpoption',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_aware_adopted',
		'label' => 'Child Aware Adopted',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'mother_presently_pregnant',
		'label' => 'Is Mother Presently Pregnant',
		'rules' => 'required|text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'mother_pregnancy_due_date',
		'label' => 'Mother\'s Pregnance Due Date',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths',
		'label' => 'Family Deaths',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_deaths_relation',
		'label' => 'Relation',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents',
		'label' => 'Family Related Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_accidents_relation',
		'label' => 'Relation to Family Accidents',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'what',
		'label' => 'what',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'when',
		'label' => 'when',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers1',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers2',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers3',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembers4',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers5',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembers6',
		'label' => 'House Member',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage1',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage2',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'hhmembersage3',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage4',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage5',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'hhmembersage6',
		'label' => 'House Members Age',
		'rules' => 'numeric|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'language_at_home',
		'label' => 'Languages Spoken at Home',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'family_activities',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'family_activities_frequent',
		'label' => 'Family Activities',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_time',
		'label' => 'TV Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'tv_whom',
		'label' => 'TV Spen with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'radio_time',
		'label' => 'Radio Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'radio_whom',
		'label' => 'Radio Time Spent With Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_time',
		'label' => 'Computer Games Time Spent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'computergames_whom',
		'label' => 'Computer Games Time Spent with Whom',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities',
		'label' => 'Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_responsibilities_what',
		'label' => 'What are you\'re Child Responsibilites',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'child_play_group',
		'label' => 'Child Play Group',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'child_play_group_frequent',
		'label' => 'Child Frequent Playgroup Frequent',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),  
	array(
		'field' => 'other_interest',
		'label' => 'Other Interest',
		'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_watch',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_boardgames',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_comp',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_reading',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_other_pt',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_spend_study',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_first_mens',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
			'field' => 'hs_signal_mens',
			'label' => '',
			'rules' => 'text_all|trim|htmlspecialchars|strip_tags'
	),
	array(
		'field' => 'al_tghome',
		'label' => 'Allow to go home independently',
		'rules' => 'required|trim|htmlspecialchars|strip_tags'
	)
];