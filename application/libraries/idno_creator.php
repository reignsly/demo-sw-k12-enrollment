<?php

class Idno_creator
{
	private $ci;
	
	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function generate_new_id($level)
	{
		/*
			format 2013-nursery-0001
			description:
			every grade level has a different base start
			ie 0001
			example
			so there will be a 2013-nursery-0001 and 2013-prep2-0001
		*/
	
		$this->ci->load->model('M_id_creator');
		$result = $this->ci->M_id_creator->get_new_id($level);
		if($result !== FALSE)
		{
			// return strlen($result->num);
			switch($result->num)
			{
				case strlen($result->num) == 1:
					$num =  str_pad($result->num,4,0,STR_PAD_LEFT);
				break;
				case strlen($result->num) == 2:
					$num =  str_pad($result->num,3,0,STR_PAD_LEFT);
				break;
				case strlen($result->num) == 3:
					$num =  str_pad($result->num,2,0,STR_PAD_LEFT);
				break;
				case strlen($result->num) == 4:
					$num = $result->num;
				break;
			}
			
			$data['num'] = $num;
			$data['cat'] = $result->cat;
			$data['year'] = $result->year;
			$data['full'] = $result->year.'-'.$result->cat.'-'.$num;
			
			return (object)$data;
		}else{
			$this->generate_new_id($level);
		}
	}



	public function generate_new_id_compressed($x)
	{
		$this->ci->load->model('M_id_creator');
		$result = $this->ci->M_id_creator->create_id($x);
		
		if($result == FALSE)
		{
			$this->generate_new_id_compressed($x);
		}else{
			return $result;
		}
	}






}